select
  nhl_player_id  , full_name , game_team_name , sum(stats_assists) as stats_assists ,  sum(stats_goals) as stats_goals , sum(stats_assists + stats_goals) as points 
from {{ ref('player_game_stats') }}  group by 1,2,3

