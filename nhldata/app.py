'''
	This is the NHL crawler.  

Scattered throughout are TODO tips on what to look for.

Assume this job isn't expanding in scope, but pretend it will be pushed into production to run 
automomously.  So feel free to add anywhere (not hinted, this is where we see your though process..)
    * error handling where you see things going wrong.  
    * messaging for monitoring or troubleshooting
    * anything else you think is necessary to have for restful nights
'''
import logging
from pathlib import Path
from datetime import datetime
from dataclasses import dataclass
import boto3
import requests
import pandas as pd
from botocore.config import Config
from dateutil.parser import parse as dateparse
import json
from typing import Dict
from contextlib import suppress
from io import StringIO
import csv

logging.basicConfig(level=logging.INFO)
LOG = logging.getLogger(__name__)

class NHLApi:
    SCHEMA_HOST = "https://statsapi.web.nhl.com/"
    VERSION_PREFIX = "api/v1"

    def __init__(self, base=None):
        self.base = base if base else f'{self.SCHEMA_HOST}/{self.VERSION_PREFIX}'


    def schedule(self, start_date: datetime, end_date: datetime) -> dict:
        ''' 
        returns a dict tree structure that is like
            "dates": [ 
                {
                    " #.. meta info, one for each requested date ",
                    "games": [
                        { #.. game info },
                        ...
                    ]
                },
                ...
            ]
        '''
        return self._get(self._url('schedule'), {'startDate': start_date.strftime('%Y-%m-%d'), 'endDate': end_date.strftime('%Y-%m-%d')})

    def boxscore(self, game_id):
        '''
        returns a dict tree structure that is like
           "teams": {
                "home": {
                    " #.. other meta ",
                    "players": {
                        $player_id: {
                            "person": {
                                "id": $int,
                                "fullName": $string,
                                #-- other info
                                "currentTeam": {
                                    "name": $string,
                                    #-- other info
                                },
                                "stats": {
                                    "skaterStats": {
                                        "assists": $int,
                                        "goals": $int,
                                        #-- other status
                                    }
                                    #-- ignore "goalieStats"
                                }
                            }
                        },
                        #...
                    }
                },
                "away": {
                    #... same as "home" 
                }
            }

            See tests/resources/boxscore.json for a real example response
        '''
        url = self._url(f'game/{game_id}/boxscore')
        return self._get(url)

    def _get(self, url, params=None):
        response = requests.get(url, params=params)
        response.raise_for_status()
        return response.json()

    def _url(self, path):
        return f'{self.base}/{path}'

@dataclass
class StorageKey:
    # TODO what propertie are needed to partition?
    gamedate: datetime
    gameid: str

    def key(self):
        ''' renders the s3 key for the given set of properties '''
        # TODO use the properties to return the s3 key
        return f'year={self.gamedate.year}/month={self.gamedate.month}/day={self.gamedate.day}/{self.gameid}.csv'

class Storage():
    def __init__(self, dest_bucket, s3_client):
        self._s3_client = s3_client
        self.bucket = dest_bucket

    def store_game(self, key: StorageKey, game_data) -> bool:
        self._s3_client.put_object(Bucket=self.bucket, Key=key.key(), Body=game_data)
        return True

class Crawler():
    def __init__(self, api: NHLApi, storage: Storage):
        self.api = api
        self.storage = storage

    def crawl(self, startDate: datetime, endDate: datetime) -> None:
      
	# NOTE the data direct from the API is not quite what we want. Its nested in a way we don't want
	#      so here we are looking for your ability to gently massage a data set. 
        #TODO error handling
        #TODO get games for dates
        games = self.api.schedule(startDate, endDate)
        output = {}
        gamesID = []
        msg = f"""
            No games found for between Start Date {startDate} and End Date {endDate} 
        """
        for dateKey,dateVal in games.items():
            if dateKey == "dates":
                if not isinstance(dateVal, list):
                    continue                   

                for all_dates in dateVal:
                    if not isinstance(all_dates, dict):
                        continue                        


                    for key1, val1 in all_dates.items():
                        if not isinstance(val1, list):
                            continue                             
  

                        for all_games in val1:
                            if not isinstance(all_games, dict):
                                continue                                


                            gameID = []
                            for key2, val2 in all_games.items():
                                if key2 == 'gamePk':
                                    gameID.append(val2)
                                elif key2 == 'gameDate':
                                    gameID.append(val2)
                            gamesID.append(gameID)

        data_lst = []
        
        for game_id, gamedate in gamesID:
            game_date = dateparse(gamedate)
            gamesStats: Dict =  self.api.boxscore(game_id)                   

            for side in ('away', 'home'):
                value = None
                with suppress(KeyError):
                    value = gamesStats['teams'][side]['players']

                if value is None:
                    msg = f"""
                        No players found for  {side}
                    """
                    LOG.exception(msg)
                    raise Exception(msg) 

                for key in value.keys():
                    data_dict = {}          
      
                    data_dict['side'] = side     

                    try:
                        goalieStats = gamesStats['teams'][side]['players'][key]['stats']['goalieStats']
                    except:
                        goalieStats = None

                    if goalieStats != None:
                        continue
                    try:
                        data_dict['player_person_id'] = gamesStats['teams'][side]['players'][key]['person']['id']
                    except:
                        continue
                    
                    try:
                        data_dict['player_person_currentTeam_name'] = gamesStats['teams'][side]['players'][key]['person']['currentTeam']['name']
                    except:
                        data_dict['player_person_currentTeam_name'] = 'Unspecified'

                    try:
                        data_dict['player_person_fullName'] = gamesStats['teams'][side]['players'][key]['person']['fullName'] 
                    except:
                        data_dict['player_person_fullName'] = 'Unspecified'
                    
                    try:
                        data_dict['player_stats_skaterStats_assists'] = gamesStats['teams'][side]['players'][key]['stats']['skaterStats']['assists'] 
                    except:
                        data_dict['player_stats_skaterStats_assists'] = 0
                    try:
                        data_dict['player_stats_skaterStats_goals'] = gamesStats['teams'][side]['players'][key]['stats']['skaterStats']['goals']
                    except:
                        data_dict['player_stats_skaterStats_goals'] = 0
                        

                    data_lst.append(data_dict)

            data_columns = ['player_person_id', 'player_person_currentTeam_name', 'player_person_fullName', 'player_stats_skaterStats_assists', 'player_stats_skaterStats_goals','side']
            
            file_buff = StringIO()
            try:
                writer = csv.DictWriter(file_buff, fieldnames=data_columns)
                writer.writeheader()
                for data in data_lst:
                    writer.writerow({k:str(v).encode('utf8').decode('utf8', errors='replace') for k,v in data.items()})
            except IOError:
                msg = f"""
                    Process failed during I/O operations
                """
                LOG.exception(msg)
                raise Exception(msg)

            StorageKeyObj = StorageKey(game_date, game_id)

       
            self.storage.store_game(StorageKeyObj, file_buff.getvalue())
            
                
def main():
    import os
    import argparse
    parser = argparse.ArgumentParser(description='NHL Stats crawler')
    parser.add_argument('-s', '--startDate')
    parser.add_argument('-e', '--endDate', default=datetime.today().strftime('%Y-%m-%d'))  
    args = parser.parse_args()

    dest_bucket = os.environ.get('DEST_BUCKET', 'output')
    startDate = datetime.strptime(args.startDate, '%Y-%m-%d')
    endDate =   datetime.strptime(args.endDate, '%Y-%m-%d')

    if endDate < startDate:
        msg = f"""
            End Date {endDate} is before Start Date {startDate}
        """
        LOG.exception(msg)
        raise Exception(msg)

    LOG.info(f"Starting the process for start Date = {startDate} and end Date = {endDate}")
    
    api = NHLApi()
    s3client = boto3.client('s3', config=Config(signature_version='s3v4'), endpoint_url=os.environ.get('S3_ENDPOINT_URL'))
    storage = Storage(dest_bucket, s3client)
    crawler = Crawler(api, storage)
    crawler.crawl(startDate, endDate)

if __name__ == '__main__':
    main()





